<?php
if($_POST['img_to_save']){
	save();
} else {
	upload();
}


function save(){
	$name = md5(mt_rand(0, 100000000));
	$img = $_POST['img_to_save'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$result = file_put_contents('./result_images/image_'.$name.'.png', base64_decode($img));
	echo $result;
}

function upload(){
	$data = array();

	if(isset($_GET['files']))
	{  
		    $error = false;
		    $files = array();

		    $uploaddir = 'uploads/';
		    foreach($_FILES as $file)
		    {
		        if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($file['name'])))
		        {
		            $files[] = $uploaddir .$file['name'];
		        }
		        else
		        {
		            $error = true;
		        }
		    }
		    $data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
		}
		else
		{
		    $data = array('success' => 'Form was submitted', 'formData' => $_POST);
		}

		echo json_encode($data);
	}

?>
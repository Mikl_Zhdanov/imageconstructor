/*
* Look through http://requirejs.org/docs/api.html#config
* in order to explore config parameters action
*/
require.config({
	packages: [
	{
		name: "aw/fifa/mascotvoting/plugins/imageConstructor",
		location: "aw/fifa/mascotvoting/plugins/imageConstructor",
		main: "mainController"
	}
	],
	paths: {
		"jquery": "vendor/jquery-2.1.4",
		"createjs" : "vendor/createjs",
		"imageConstructor/start": "aw/fifa/mascotvoting/plugins/imageConstructor/start",
	}
});

require(["imageConstructor/start"], function(){
});

require.onError = function (err) {
	console.log(err);
	throw err;
};

//класс холста

define(["jquery", './abstractSheet', "createjs"], function($, AbstractSheet) {

  var Sheet = function() {
    this._context;
    this._errorText;
    this._stage;
    this._elements = [];
  };

  Sheet.prototype = Object.create(AbstractSheet.prototype);

  S = Sheet;
  p = S.prototype;

  p.constructor = AbstractSheet;

  p.setContext = function(context){
    if (this._context != context) {
      this._context = context;
      this._isChanged = true;
      this._update();        
    } else {
      return true;
    }
  };

  p.setErrorText = function(errorText){
    if (this._errorText != errorText) {
      this._errorText = errorText;
      this._isChanged = true;
      this._update();        
    } else {
      return true;
    }
  };

  p.getContext = function(){
    return this._context;
  };

  p.getErrorText = function(){
    return this._errorText;
  };

  p.init = function(id, x, y, rotation, scale, width, height, context, errorText){
    this._id = id;
    this._x = x;
    this._y = y;
    this._rotation = rotation;
    this._scale = scale;
    this._width = width;
    this._height = height;
    this._context = context;
    this._errorText = errorText;
  };
  
  p.addElement = function(Element, id) {
    this._elements[id] = Element;
  };

  p.removeElement = function(elementId) {
    this._stage.removeChild(this._elements[elementId]);
    delete this._elements[elementId];
    this._stage.update();
  };

  p.removeAllElements = function() {
    this._elements.splice(0,this._elements.length);
  };

  p.render = function(){
    for(var i in this._elements){
      this._stage.addChild(this._elements[i]);
      this._stage.update();
    } 
  };

  p.drawCanvas = function(){
    $(".canvas_block").append("<canvas id='" + this._id + "' style='top:" + this._y + "px ; left:" + this._x + "px; ' height='" + this._height + "' width='" + this._width + "'>" + this._errorText + "</canvas>");
    this._stage = new createjs.Stage(this._id);
    this._stage.enableDOMEvents(true);
  };

  p.getStage = function(){
    return this._stage;
  };

  p.saveCanvasAsImage = function(){
      var canvas = document.getElementById(this._id);
      var dataURL = canvas.toDataURL();
      return dataURL;
  }

  return Sheet;

});

// абстрактный класс холста

define(["../abstracts/abstractObject"], function(AbstractObject) {

    function AbstractSheet() {
        this._width;
        this._height;
        this._id;
    };

    AbstractSheet.prototype = Object.create(AbstractObject.prototype);

    var a = AbstractSheet;
    var p = a.prototype;

    p.constructor = AbstractObject;

    p.setWidth = function(width) {
        if (this._width != width) {
            this._width = width;
            this._isChanged = true;
            this._update();        
        } else {
            return true;
        }
    };

    p.setHeight = function(height) {
        if (this._height != height) {
            this._height = height;
            this._isChanged = true;
            this._update();      
        } else {
            return true;
        }
    };

    p.getWidth = function() {
        return this._width;
    };

    p.getHeight = function() {
        return this._height;
    };

    p.setId = function(id) {
        if (this._id != id) {
            this._id = id;
            this._isChanged = true;
            this._update();        
        } else {
            return true;
        }
    }

    p.getId = function() {
        return this._id;
    }

    return a;

});

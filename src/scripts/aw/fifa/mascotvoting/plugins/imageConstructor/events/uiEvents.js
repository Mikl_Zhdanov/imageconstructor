// собития UI

define(["createjs"], function() {

    function UIEvents() {
    };

    UIEvents.prototype = Object.create(createjs.Event.prototype);

    var U = UIEvents;
    var p = U.prototype;

    p.constructor = createjs.Event;

    U.ADD_NEW_ELEMENT = "ADD_NEW_ELEMENT";
    
    U.ID_CHANGED = "ID_CHANGED";
    U.POSITION_CHANGED = "POSITION_CHANGED";
    U.SCALE_CHANGED = "SCALE_CHANGED";
    U.ROTATION_CHANGED = "ROTATION_CHANGED";
    U.REMOVE = "REMOVE";

    return U;

});
define([
        './abstractElementFactory',
        '../element/templates/templateShape',
        '../element/templates/templateImage',
        '../element/userImage'
    ], function(
        AbstractElementFactory,
        TemplateShape, 
        TemplateImage,
        UserImage
    ) {

    function ElementFactory(){
        this._elements = {};
    };

    ElementFactory.prototype = Object.create(AbstractElementFactory.prototype);

    var F = ElementFactory;
    var p = F.prototype;

    p.constructor = AbstractElementFactory;

    p.makeElement = function(id, type, imageURL){
        var elem;

        if (this._elements[type]){
            elem = this._elements[type].pop();
            if(this._elements[type].length == 0){
                delete this._elements[type];
            }
        } else {
            switch (type)
            {
                case "UserImage": 
                    var image = new Image();
                    image.src = "../server_caps/" + imageURL;
                    elem = new TemplateImage();
                    elem.init(id, 0, 1, image.width, image.height, image);
                    console.log(elem);
                break;

                case "JsIcon": 
                    var image = new Image();
                    image.src = './img/icon-logo-js.png';
                    elem = new TemplateImage();
                    elem.init(id, 0, 1, 49, 49, image);
                break;
     
                case "HtmlIcon": 
                    var image = new Image();
                    image.src = './img/html.png';
                    elem = new TemplateImage();
                    elem.init(id, 0, 1, 64, 64, image);
                break;

                case "CssIcon": 
                    var image = new Image();
                    image.src = './img/css_normal.png';
                    elem = new TemplateImage();
                    elem.init(id, 0, 1, 256, 256, image);
                break;

                case "GulpIcon": 
                    var image = new Image();
                    image.src = './img/gulp-js.png';
                    elem = new TemplateImage();
                    elem.init(id, 0, 1, 64, 64, image);
                break;

                case "GreyRectangle":
                    elem = new TemplateShape();
                    elem.init(id, 0, 1, 100,100);                
                break;
               
                default: console.log("makeElement failed");  
            }
        }

        elem.constructorType = type;

        return elem;
    };

    p.detachElement = function(elem)
    {       
        if (!elem.constructorType) return;
        var type = elem.constructorType;
        this._elements[type] = this._elements[type] || [];
        this._elements[type].push(elem);
    }

 	return F;

});

define([], function() {

    function AbstractElementFactory(){
    };

    var A = AbstractElementFactory;
    var p = A.prototype;

    p.makeElement = function(){
    };

    p.detachElement = function(){
    };

 	return A;

});

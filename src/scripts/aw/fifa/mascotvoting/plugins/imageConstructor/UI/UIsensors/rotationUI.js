// UI изменения угла поворота
define(["jquery", "../objectUI"], function($, ObjectUI) {

    function RotationUI() {
    };

    RotationUI.prototype = Object.create(ObjectUI.prototype);

    var U = RotationUI;
    var p = U.prototype;

	p.constructor = ObjectUI;

    p.init = function() {
        var that = this;

        $('.rotationUI').on("mousedown", function(event){
            $(document).unbind('mouseup');
            $(document).unbind('mousemove');

            $(this).trigger('click');
            $('.rotationUI').attr("move", true);

            $(document).on("mouseup", function(event){
                $('.rotationUI').attr("move", false);
            });

            $(document).on("mousemove", function(event){
                if($('.rotationUI').attr("move") == "true"){ 

                    var a = (event.pageX - that._linkedX - that._linkedWidth * that._linkedScale/2);
                    var b = (event.pageY - that._linkedY - that._linkedHeight * that._linkedScale/2);
                    var fixRotation = 0;

                    if(a != 0){
                        var rotationTg = b/a;
                    };

                    if(a < 0){
                        fixRotation = -45;
                    } else {
                        fixRotation = 135;
                    };

                    var rotation = Math.atan(rotationTg) * 57.32 + fixRotation;

                    that.setLinkedRotation(rotation);           
                    that.dispatchRotationChanged();
                }
            });

        });

    };

    return U;

});
// абстрактный класс UI

define(["../abstracts/abstractObject", "createjs"], function(AbstractObject) {

    function AbstractUI() {
    };

    AbstractUI.prototype = Object.create(AbstractObject.prototype);

    var A = AbstractUI;
    var p = A.prototype;

    p.constructor = AbstractObject;

    createjs.EventDispatcher.initialize(p);

    return A;

});
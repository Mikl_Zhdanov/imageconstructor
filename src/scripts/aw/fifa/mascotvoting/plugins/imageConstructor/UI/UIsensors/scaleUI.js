// UI изменения угла поворота
define(["jquery", "../objectUI"], function($, ObjectUI) {

    function ScaleUI() {
    };

    ScaleUI.prototype = Object.create(ObjectUI.prototype);

    var U = ScaleUI;
    var p = U.prototype;

    p.constructor = ObjectUI;

    p.init = function() {
        var that = this;

        $('.scaleUI').on("mousedown", function(event){
            $(document).unbind('mouseup');
            $(document).unbind('mousemove');
            
            $(this).trigger('click');
            $('.scaleUI').attr("move", true);

            $(document).mouseup(function(event){
                $('.scaleUI[move = "true"]').trigger('click');
                $('.scaleUI').attr("move", false);
            });

            $(document).mousemove(function(event){
                if($('.scaleUI').attr("move") == "true"){
                    $('.scaleUI[move = "true"]').trigger('click');
                    var x0 = that._linkedX;
                    var y0 = that._linkedY;
                    var x = event.pageX + 20;
                    var y = event.pageY + 20;
                    var a = that._linkedWidth;
                    var b = that._linkedHeight;

                    var scale = (Math.sqrt(Math.pow((x - (x0 + (a/2))), 2) + Math.pow((y - (y0 + (b/2))), 2))) / (Math.sqrt((a*a) + (b*b)));

                    if(scale < 0.1){
                        scale = 0.1;
                    }

                    that.setLinkedScale(scale);
                    that.dispatchScaleChanged();
                   
                }
            });
        });

    };

    return U;

});
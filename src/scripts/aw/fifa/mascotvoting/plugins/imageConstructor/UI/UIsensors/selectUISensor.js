// UI изменения позиции
define(["jquery", "../selectUI"], function($, SelectUI) {

    function SelectUISensor() {
        this._state = "stop";
    };

    SelectUISensor.prototype = Object.create(SelectUI.prototype);

    var S = SelectUISensor;
    var p = S.prototype;

    p.constructor = SelectUI;

    p.init = function() {
        var that = this;

        $('.selectUIItem').on("mousedown", function(event) {
            $(document).unbind('mouseup');
            $(document).unbind('mousemove');

            $(this).trigger('click');
            $(this).attr("data-move", true);
            $(this).css("pointer-events","none");
            that._state = "move";
            that.setType($(this).attr("data-type"));


            $(document).on("mouseup", 'canvas', function(event) {
                if(that._state == "move"){
                    $('.selectUIItem[data-move = "true"]').css("pointer-events", "auto");
                    $('.selectUIItem[data-move = "true"]').css("top", "inherit");
                    $('.selectUIItem[data-move = "true"]').css("left", "inherit");
                    $('.selectUIItem').attr("data-move", false);
                    that.dispatchAddElement();
                }

                $('.selectUIItem').attr("data-move", false);
                that._state = "stop";
            });

            $(document).on("mouseup",function(event) {
                if(that._state == "move"){
                    $('.selectUIItem[data-move = "true"]').css("pointer-events", "auto");
                    $('.selectUIItem[data-move = "true"]').css("top", "inherit");
                    $('.selectUIItem[data-move = "true"]').css("left", "inherit");
                    $('.selectUIItem').attr("data-move", false);
                }

                $('.selectUIItem').attr("data-move", false);
                that._state = "stop";
            });

            $(document).on("mousemove", function(event) {
                if(that._state == "move"){

                    var x = event.pageX - 20;
                    var y = event.pageY - 20;
                    that.setPosition(x, y);

                    $('.selectUIItem[data-move = "true"]').css("top", y + "px");
                    $('.selectUIItem[data-move = "true"]').css("left", x + "px");

                }
            });

        });
    };

    return S;

});

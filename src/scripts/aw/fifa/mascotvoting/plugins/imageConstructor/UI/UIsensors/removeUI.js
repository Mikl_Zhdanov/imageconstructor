// UI изменения позиции
define(["jquery", "../objectUI"], function($, ObjectUI) {

    function RemoveUI() {
    };

    RemoveUI.prototype = Object.create(ObjectUI.prototype);

    var U = RemoveUI;
    var p = U.prototype;

	p.constructor = ObjectUI;

    p.init = function() {
        var that = this;

        $(".deleteUI").on("click", function(){
            that.dispatchRemove();
        });

    };

    return U;

});
// UI объекта
define(["jquery", "./abstractUI", "../events/uiEvents"], function($, AbstractUI, UIEvents) {

    function ObjectUI() {
        this._linkedId;
        this._linkedX;
        this._linkedY;
        this._linkedRotation;
        this._linkedScale;
        this._linkedWidth;
        this._linkedHeight;
    };

    ObjectUI.prototype = Object.create(AbstractUI.prototype);

    var O = ObjectUI;
    var p = O.prototype;

	p.constructor = AbstractUI;

    p.setLinkedId = function(linkedId) {
        if (this._linkedId != linkedId) {
            this._linkedId = linkedId;
            this._isChanged = true;
            this._update();        
        } else {
            return true;
        }
    };

    p.getLinkedId = function() {
        return this._linkedId;
    };

    p.setLinkedPosition = function(x, y) {
        if(this._linkedX != x || this._linkedY != y){
            this._linkedX = x;
            this._linkedY = y;
            this._isChanged = true;            
            this._update();
        } else {
            return true;
        }
    }; 

    p.setLinkedStartBounds = function(width, height) {
        if(this._linkedWidth != width || this._linkedHeight != height){
            this._linkedWidth = width;
            this._linkedHeight = height;
            this._isChanged = true;            
            this._update();
        } else {
            return true;
        }
    }; 

    p.setLinkedRotation = function(rotation) {
        if(this._linkedRotation != rotation){
            this._linkedRotation = rotation;
            this._isChanged = true;
            this._update();        
        } else {
            return true;
        }
    }; 

    p.setLinkedScale = function(scale) {
        if(this._linkedScale != scale){ 
            this._linkedScale = scale;
            this._isChanged = true;
            this._update();          
        } else {
            return true;
        }
    }; 

    // Event dispatch functions
    p.dispatchIdChanged = function(){
        this.dispatchEvent(new CustomEvent(UIEvents.ID_CHANGED, {detail: {id: this._linkedId}}));
    };

    p.dispatchRemove = function(){
        this.dispatchEvent(new CustomEvent(UIEvents.REMOVE, {detail: {id: this._linkedId}}));
    };

    p.dispatchPositionChanged = function(){
        this.dispatchEvent(new CustomEvent(UIEvents.POSITION_CHANGED, {detail: {id: this._linkedId, x: this._linkedX, y: this._linkedY}}));
    };

    p.dispatchScaleChanged = function(){
        this.dispatchEvent(new CustomEvent(UIEvents.SCALE_CHANGED, {detail: {id: this._linkedId, scale: this._linkedScale}}));
    };

    p.dispatchRotationChanged = function(){
        this.dispatchEvent(new CustomEvent(UIEvents.ROTATION_CHANGED, {detail: {id: this._linkedId, rotation: this._linkedRotation}}));
    };

    return O;

});
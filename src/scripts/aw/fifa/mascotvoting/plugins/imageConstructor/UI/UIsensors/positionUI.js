// UI изменения позиции
define(["jquery", "../objectUI"], function($, ObjectUI) {

    function PositionUI() {
    };

    PositionUI.prototype = Object.create(ObjectUI.prototype);

    var U = PositionUI;
    var p = U.prototype;

	p.constructor = ObjectUI;

    p.init = function(element) {
        var that = this;

        element.on("stagemouseup", function() {
           that.setLinkedId(null);
           that.dispatchIdChanged();
        });

        element.on("click", function(event){
            that.setLinkedId(event.target.id);
            that.dispatchIdChanged();
        });

        element.on("pressmove", function(event){
            that.setLinkedId(event.target.id);
            that.dispatchIdChanged();

            that.setLinkedPosition(event.stageX - that._linkedWidth*that._linkedScale/2, event.stageY - that._linkedHeight*that._linkedScale/2);
            that.dispatchPositionChanged();
        });

    };

    return U;

});
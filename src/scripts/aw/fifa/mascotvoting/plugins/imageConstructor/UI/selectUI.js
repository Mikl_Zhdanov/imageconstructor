// UI объекта
define(["jquery", "./abstractUI", "../events/uiEvents"], function($, AbstractUI, UIEvents) {

    function SelectUI() {
        this._type;
        this._mountingX;
        this._mountingY;
    };

    SelectUI.prototype = Object.create(AbstractUI.prototype);

    var O = SelectUI;
    var p = O.prototype;

	p.constructor = AbstractUI;

    p.setPosition = function(x, y) {
        if(this._mountingX != x || this._mountingY != y){
            this._mountingX = x;
            this._mountingY = y;
            this._isChanged = true;            
            this._update();
        } else {
            return true;
        }
    }; 

    p.setType = function(type) {
        if(this._type != type){
            this._type = type;
            this._isChanged = true;            
            this._update();
        } else {
            return true;
        }        
    }

    // Event dispatch functions

    p.dispatchAddElement = function(){
        this.dispatchEvent(new CustomEvent(UIEvents.ADD_NEW_ELEMENT, {detail: {type: this._type, x: this._mountingX, y: this._mountingY}}));
    };

    return O;

});
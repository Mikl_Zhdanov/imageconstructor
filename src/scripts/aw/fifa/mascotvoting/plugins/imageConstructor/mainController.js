define(
	[
		'jquery',
		'./sheet/sheet',
		'./factory/elementFactory',
		'./element/templates/templateShape',
		'./element/templates/templateImage',
		'./element/userImage',
		'./UI/UIsensors/positionUI',
		'./UI/UIsensors/rotationUI',
		'./UI/UIsensors/scaleUI',
		'./UI/UIsensors/removeUI',
		'./UI/UIsensors/selectUISensor',
		'createjs'
	],
	function(
		$,
		Sheet,
		ElementFactory,
		TemplateShape,
		TemplateImage,
		UserImage,
		PositionUI,
		RotationUI,
		ScaleUI,
		RemoveUI,
		SelectUI
	){

	function mainController(){
		this._elementCounter = 0;
	};

	mainController.prototype.startImageConstructor = function() {

		this.templateShape = new Array;
		this.sheet = new Sheet();
		this.sheet.init("firstCanvas", 0, 0, 0, 1, 500, 300, "2d", "Обновите браузер");
		this.sheet.drawCanvas();

		this.elementFactory = new ElementFactory();
		this.sheet.render();

		//UI init
		this.selectUI = new SelectUI();
		this.positionUi = new PositionUI();
		this.rotationUI = new RotationUI();
		this.scaleUI = new ScaleUI();
		this.removeUI = new RemoveUI();
		
		this.selectUI.init();
		this.positionUi.init(this.sheet.getStage());
		this.rotationUI.init();
		this.scaleUI.init();
		this.removeUI.init();
		
		this.addSelectListeners(this.selectUI);
		this.addObjectListeners(this.positionUi);
		this.addObjectListeners(this.rotationUI);
		this.addObjectListeners(this.scaleUI);
		this.addObjectListeners(this.removeUI);
		this.addDOMEventListeners();
	};

	mainController.prototype.addDOMEventListeners = function(){
		var that = this;
		var files;

		$('.btn.save').click(function(){
			var tmpImgUrl = that.sheet.saveCanvasAsImage()
			document.getElementById('canvasImg').src = tmpImgUrl;

			$.ajax({
			   method: "POST",	
			   url:"../server_caps/uploader.php", 
			   data: "img_to_save=" + tmpImgUrl, 
			   success:function(success){
			      console.log("успех!" + success);
			   }
			});
		});

		$('input[type=file]').on('change', prepareUpload);

		$('.btn.load').click(uploadFiles);

		function prepareUpload(event){
		  files = event.target.files;
		}

		function uploadFiles(){
		    var data = new FormData();
		    $.each(files, function(key, value)
		    {
		        data.append(key, value);
		    });

		    $.ajax({
		        url: '../server_caps/uploader.php?files',
		        type: 'POST',
		        data: data,
		        cache: false,
		        dataType: 'json',
		        processData: false,
		        contentType: false,
		        success: function(success)
		        {
		            that.templateShape[that._elementCounter] = that.elementFactory.makeElement(that._elementCounter, "UserImage", success["files"][success["files"].length - 1]);
					that.templateShape[that._elementCounter].setPosition(10, 10);
					that.sheet.addElement(that.templateShape[that._elementCounter].getAvatar(), that.templateShape[that._elementCounter].getId());
					that._elementCounter++;
					that.sheet.render();
		        },
		        error: function()
		        {
		            console.log('error: ');
		        }
		    });
		}
	}

	mainController.prototype.addObjectListeners = function(objectUI){
		var that = this;

		objectUI.addEventListener("ID_CHANGED", changeId);
		objectUI.addEventListener("POSITION_CHANGED", changePosition);
		objectUI.addEventListener("SCALE_CHANGED", changeScale);
		objectUI.addEventListener("ROTATION_CHANGED", changeRotation);
		objectUI.addEventListener("REMOVE", remove);

		function changeId(event){
			if(event.detail.id !== null && event.detail.id !== undefined){
				updateUi(event.detail.id);
				changeGraphicUi(event.detail.id);
				that.sheet.render();
			} else {
				changeGraphicUi(event.detail.id);
				that.sheet.render();			
			}
		};

		function changePosition(event){
			that.templateShape[event.detail.id].setPosition(event.detail.x, event.detail.y);
			updateUi(event.detail.id);
			changeGraphicUi(event.detail.id)
			that.sheet.render();
		};

		function changeScale(event){
			that.templateShape[event.detail.id].setScale(event.detail.scale);
			updateUi(event.detail.id);
			changeGraphicUi(event.detail.id);
			that.sheet.render();
		};

		function changeRotation(event){
			that.templateShape[event.detail.id].setRotation(event.detail.rotation);		
			updateUi(event.detail.id);
			changeGraphicUi(event.detail.id);
			that.sheet.render();
		};

		function remove(event){
			that.templateShape[event.detail.id].reset();
			that.elementFactory.detachElement(that.templateShape[event.detail.id]);
			updateUi(null);
			changeGraphicUi(null);
			that.sheet.removeElement(event.detail.id);
			that.sheet.render();
		};

		//обновляем параметры выбранного элемента в объектах UI
		function updateUi(id){
			if(id !== null && id !== undefined){
				that.positionUi.setLinkedId(id);
				that.positionUi.setLinkedPosition(that.templateShape[id].getX(), that.templateShape[id].getY());
				that.positionUi.setLinkedScale(that.templateShape[id].getScale());
				that.positionUi.setLinkedRotation(that.templateShape[id].getRotation());
				that.positionUi.setLinkedStartBounds(that.templateShape[id].getWidth(), that.templateShape[id].getHeight());
			
				that.scaleUI.setLinkedId(id);
				that.scaleUI.setLinkedPosition(that.templateShape[id].getX(), that.templateShape[id].getY());
				that.scaleUI.setLinkedScale(that.templateShape[id].getScale());
				that.scaleUI.setLinkedRotation(that.templateShape[id].getRotation());
				that.scaleUI.setLinkedStartBounds(that.templateShape[id].getWidth(), that.templateShape[id].getHeight());

				that.rotationUI.setLinkedId(id);
				that.rotationUI.setLinkedPosition(that.templateShape[id].getX(), that.templateShape[id].getY());
				that.rotationUI.setLinkedScale(that.templateShape[id].getScale());
				that.rotationUI.setLinkedRotation(that.templateShape[id].getRotation());
				that.rotationUI.setLinkedStartBounds(that.templateShape[id].getWidth(), that.templateShape[id].getHeight());

				that.removeUI.setLinkedId(id);
				that.removeUI.setLinkedPosition(that.templateShape[id].getX(), that.templateShape[id].getY());
				that.removeUI.setLinkedScale(that.templateShape[id].getScale());
				that.removeUI.setLinkedRotation(that.templateShape[id].getRotation());
				that.removeUI.setLinkedStartBounds(that.templateShape[id].getWidth(), that.templateShape[id].getHeight());
			};
		};

		//Изменение вида графического UI, надо вызывать каждый раз, когда срабатывает какой нибудб eventListener
		function changeGraphicUi(id){
			if(id !== null && id !== undefined){	

				$('.scaleUI').show();
				$('.rotationUI').show();
				$('.deleteUI').show();
				$('.selectionShape').show();

				$('.selectionShape').css("width", that.templateShape[id].getWidth()*that.templateShape[id].getScale());
				$('.selectionShape').css("height", that.templateShape[id].getHeight()*that.templateShape[id].getScale());
				$('.selectionShape').css("transform", "rotate(" + that.templateShape[id].getRotation() + "deg)");

				$('.selectionShape').css("top", that.templateShape[id].getY());
				$('.selectionShape').css("left", that.templateShape[id].getX());

				$('.rotationUI').css("top", that.templateShape[id].getY() - $('.scaleUI').width());
				$('.rotationUI').css("left", that.templateShape[id].getX() - $('.scaleUI').height());
				$('.rotationUI').css("transform", "rotate(" + that.templateShape[id].getRotation() + "deg)");
				$('.rotationUI').css("transform-origin","" + (that.templateShape[id].getWidth()*that.templateShape[id].getScale()/2 + 20) + "px " + (that.templateShape[id].getHeight()*that.templateShape[id].getScale()/2 + 20) + "px");
				
				$('.deleteUI').css("top", that.templateShape[id].getY() - $('.deleteUI').width());
				$('.deleteUI').css("left", that.templateShape[id].getX() - $('.deleteUI').height());
				$('.deleteUI').css("transform", "rotate(" + (that.templateShape[id].getRotation() + 90) + "deg)");
				$('.deleteUI').css("transform-origin","" + (that.templateShape[id].getWidth()*that.templateShape[id].getScale()/2 + 20) + "px " + (that.templateShape[id].getHeight()*that.templateShape[id].getScale()/2 + 20) + "px");

				$('.scaleUI').css("top", that.templateShape[id].getY() - $('.deleteUI').width());
				$('.scaleUI').css("left", that.templateShape[id].getX() - $('.deleteUI').height());
				$('.scaleUI').css("transform", "rotate(" + (that.templateShape[id].getRotation() + 180) + "deg)");
				$('.scaleUI').css("transform-origin","" + (that.templateShape[id].getWidth()*that.templateShape[id].getScale()/2 + 20) + "px " + (that.templateShape[id].getHeight()*that.templateShape[id].getScale()/2 + 20) + "px");	
									
			} else {

				$('.deleteUI').hide();
				$('.scaleUI').hide();
				$('.rotationUI').hide();
				$('.selectionShape').hide();

			};
		};
	};

	mainController.prototype.addSelectListeners = function(objectUI){
		var that = this;

		objectUI.addEventListener("ADD_NEW_ELEMENT", addElement);

		function addElement(event){
			that.templateShape[that._elementCounter] = that.elementFactory.makeElement(that._elementCounter, event.detail.type);
			that.templateShape[that._elementCounter].setPosition(event.detail.x, event.detail.y);
			that.sheet.addElement(that.templateShape[that._elementCounter].getAvatar(), that.templateShape[that._elementCounter].getId());

			that._elementCounter++;
			updateUi(event.detail.type);
			changeGraphicUi(event.detail.type)
			that.sheet.render();
		};

		//обновляем параметры выбранного элемента в объектах UI
		function updateUi(type){
		};

		//Изменение вида графического UI, надо вызывать каждый раз, когда срабатывает какой нибудь eventListener
		function changeGraphicUi(type){
		};
	};

	mainController.prototype.removeListeners = function(objectUI){
		objectUI.removeAllEventListeners();
	};

	return mainController;

})
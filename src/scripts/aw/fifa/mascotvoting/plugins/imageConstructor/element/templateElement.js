define(["jquery", './abstractElement', "createjs"], function($, AbstractElement) {

	function TemplateElement() {
        this._element;
        this._avatar;
        this._image;
        this._width;
        this._height;
	};

	TemplateElement.prototype = Object.create(AbstractElement.prototype);

    T = TemplateElement;
    p = T.prototype;

	p.constructor = AbstractElement;

    p.init = function(id,rotation, scale, width, height, image){
        this._id = id;
        this._rotation = rotation;
        this._scale = scale;
        this._isChanged = true;
        this._rotationChanged = true;
        this._scaleChanged = true;
        this._positionChanged = true;
        this._width = width;
        this._height = height;
        this._image = image;
        this._constructElement();
        this._update();
    };

    p._getElement = function(){
        return this._element;
    };

    p.getAvatar = function(){
        return this._avatar;
    };

    p._updateElement = function(){
        this._element.regX = this._width/2;
        this._element.regY = this._height/2;    
        this._element.rotation = this._rotation; 
        this._element.scaleX = this._scale;
        this._element.scaleY = this._scale;
    };

    p._updateAvatar = function(){
        this._avatar = this._element;
        this._avatar.id = this._id;
    };

    p._update = function(){
        
        var elementChanged; 

        if(this._rotationChanged || this._scaleChanged){
            this._updateElement();
            elementChanged = true;
            this._rotationChanged = false;
            this._scaleChanged = false;
        };

        if(elementChanged){
            this._updateAvatar();
        };

        if(elementChanged || this._positionChanged){
            this._avatar.x = this._x + this._width*this._scale/2;
            this._avatar.y = this._y + this._height*this._scale/2;  
        };
        
    };

    p.reset = function(){
        this._x = 0;
        this._y = 0;
        this._rotation = 0;
        this._scale = 1;
        this._isChanged = true;
        this._rotationChanged = true;
        this._scaleChanged = true;
        this._positionChanged = true;
        this._update();
    }

 	return T;

});

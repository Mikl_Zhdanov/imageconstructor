define(["jquery", '../templateElement', "createjs"], function($, TemplateElement) {

	function TemplateImage() {
	};

	TemplateImage.prototype = Object.create(TemplateElement.prototype);

    T = TemplateImage;
    p = T.prototype;

	p.constructor = TemplateElement;

    p._constructElement = function(){
        this._element = new createjs.Bitmap(this._image);
    };

 	return T;

});

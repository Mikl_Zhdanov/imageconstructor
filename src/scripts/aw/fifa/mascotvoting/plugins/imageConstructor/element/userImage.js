define(["jquery", './abstractElement', "createjs"], function($, AbstractElement) {

	function UserImage() {
        this._image;
	};

	UserImage.prototype = Object.create(AbstractElement.prototype);

    U = UserImage;
    p = U.prototype;

	p.constructor = AbstractElement;

    p.init = function(id, x, y, rotation, scale, image){
        this._id = id;
        this._x = x;
        this._y = y;
        this._rotation = rotation;
        this._scale = scale;
        this._image = image;
    };
    
    p.setImage = function(image){
        if (this._image != image) {
            this._image = image;
            this._isChanged = true;
            this._update();        
        } else {
            return true;
        }
    };

    p.getImage = function(){
        return this._image;
    };

 	return U;

});

define(["jquery", '../templateElement', "createjs"], function($, TemplateElement) {

    function TemplateShape() {
    };

    TemplateShape.prototype = Object.create(TemplateElement.prototype);

    T = TemplateShape;
    p = T.prototype;

    p.constructor = TemplateElement;

    p._constructElement = function(){
        this._element = new createjs.Shape();
        this._element.graphics.beginFill("lightgrey").drawRect(0, 0, this._width, this._height).endFill();
    };

    return T;

});
// абстрактный класс картинки
define(["../abstracts/abstractObject"], function(AbstractObject) {

    function AbstractElement() {
        this._id;
        this._width;
        this._height;
    };

    AbstractElement.prototype = Object.create(AbstractObject.prototype);

    var a = AbstractElement;
    var p = a.prototype;

    p.constructor = AbstractObject;

    p.setId = function(id) {
        if (this._id != id) {
            this._id = id;
            this._isChanged = true;
            this._update();        
        } else {
            return true;
        }
    }

    p.getId = function() {
        return this._id;
    }
    
    p.setShapeBounds = function(width, height){
        if (this._width != width || this._height != height) {
            this._width = width;
            this._height = height;
            this._isChanged = true;
            this._update();        
        } else {
            return true;
        }
    }

    p.getWidth = function(){
        return this._width;
    }

    p.getHeight = function(){
        return this._height;
    }

    return a;

});

// абстрактный класс оъекта

define([], function() {


    function AbstractObject() {
        this._x;
        this._y;
        this._rotation;
        this._scale;
        this._isChanged;
        this._rotationChanged;
        this._scaleChanged;
        this._positionChanged;
    };

    AbstractObject.prototype = Object.create(Object.prototype);

    var a = AbstractObject;
    var p = a.prototype;

    p.constructor = Object;

    p.setPosition = function(x, y) {
        if(this._x != x || this._y != y){
            this._x = x;
            this._y = y;
            this._positionChanged = true;
            this._isChanged = true;            
            this._update();
        } else {
            return true;
        }
    }; 

    p.setRotation = function(rotation) {
        if(this._rotation != rotation){
            this._rotation = rotation;
            this._rotationChanged = true;
            this._isChanged = true;
            this._update();        
        } else {
            return true;
        }
    }; 

    p.setScale = function(scale) {
        if(this._scale != scale){ 
            this._scale = scale;
            this._scaleChanged = true;
            this._isChanged = true;
            this._update();          
        } else {
            return true;
        }
    }; 

    p.getX = function(){
        return this._x;
    };

    p.getY = function(){
        return this._y;
    };

    p.getRotation = function(){
        return this._rotation;
    };

    p.getScale = function(){
        return this._scale;
    };

    p._update = function(){
    }

    return a;

});
//переменные

var gulp = require('gulp'), // Сообственно Gulp JS
  uglify = require('gulp-uglify'), // Минификация JS
  concat = require('gulp-concat'), // Склейка файлов
  imagemin = require('gulp-imagemin'), // Минификация изображений
  csso = require('gulp-csso'), // Минификация CSS
  sass = require('gulp-sass'), // Конверстация SASS (SCSS) в CSS
  clean = require('gulp-clean'), // очистка директорий
  autoprefixer = require('gulp-autoprefixer'), //автоматическая расстановка префиксов
  runSequence = require('run-sequence'), //синхронизация выполнения задач
  rigger = require('gulp-rigger') //использование шаблонов
  csslint = require('gulp-csslint');

//пути
var path = {
  build: {
    html: 'build/',
    js: 'build/js/',
    css: 'build/css/',
    img: 'build/img/',
    fonts: 'build/fonts/'
  },
  src: {
    html_template: 'src/views/index.html',
    lint_config:'lint_config.json'
  },
  watch: {
    html: 'src/views/**/*.html',
    js: 'src/scripts/**/*.js',
    style: 'src/styles/**/*.scss',
    img: 'src/images/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  clean: 'build'
};


// Задача clean
gulp.task('clean', function () {
  return gulp.src('build', {read: false})
      .pipe(clean());
});

// Задача html
gulp.task('html', function() {
  gulp.src(path.watch.html)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html));
});

// Задача sass
gulp.task('sass', function() {
  gulp.src(path.watch.style)
    .pipe(sass().on('error', sass.logError))
    .pipe(csso())
    .pipe(autoprefixer({
      browsers: ['last 1 versions'],
      cascade: false
    }))
    .pipe(gulp.dest(path.build.css));
    // .pipe(csslint())
    // .pipe(csslint.reporter());
});

// Задача js
gulp.task('js', function() {
  gulp.src(path.watch.js)
    .pipe(gulp.dest(path.build.js))
});

// Задача images
gulp.task('images', function() {
  gulp.src(path.watch.img)
    .pipe(imagemin())
    .pipe(gulp.dest(path.build.img))

});

// Задача watch
gulp.task('watch', function() {
  gulp.watch(path.watch.html, ['html']);
  gulp.watch(path.watch.style, ['sass']);
  gulp.watch(path.watch.js, ['js']);
  gulp.watch(path.watch.img, ['images']);
});

//список задач 
var tasks = {
  development: ['html', 'sass', 'js', 'images', 'watch']
    // production: ['html', 'css', 'js', 'images', 'video', 'fonts']
}

var env = (process.env.NODE_ENV == 'production') ? 'production' : 'development';

gulp.task('default', function(callback) {
  runSequence('clean', tasks[env], callback);
});
